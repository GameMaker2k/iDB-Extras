<?php
/*
    $ThemeInfo - Name: IPB Alt Theme - Author: eternalSHADOW.com $
	$FileInfo: info.php - Last Update: 4/10/2022 SVN 961 - Author: cooldude2k $
*/
$ThemeInfo = array();
$ThemeInfo['ThemeName'] = "IPB Alt Theme";
$ThemeInfo['ThemeMaker'] = "eternalSHADOW.com";
$ThemeInfo['ThemeVersion'] = "0.4.7";
$ThemeInfo['ThemeVersionType'] = "Alpha";
$ThemeInfo['ThemeSubVersion'] = "SVN 961";
$ThemeInfo['MakerURL'] = "http://eternalshadow.com/";
$ThemeInfo['CopyRight'] = "%{ThemeName}T was made by <a href=\"%{MakerURL}T\" title=\"%{ThemeMaker}T\">%{ThemeMaker}T</a>";
?>