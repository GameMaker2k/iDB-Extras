<?php
/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Revised BSD License for more details.

    Copyright 2004-2022 Jcink - https://launchpad.net/tfbb
    Copyright 2004-2022 Jcink - http://jcink.com/
    $ThemeInfo - Name: TFBB Theme - Author: jcink $
    $FileInfo: index.php - Last Update: 4/10/2022 SVN 961 - Author: jcink $
*/
header('Location: ../../index.php');
?>